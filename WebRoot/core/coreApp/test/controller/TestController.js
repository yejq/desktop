/**
*程序主控制器
*/
Ext.define("core.test.controller.TestController", {
	extend : "Ext.app.Controller",
	init : function(){
		var self = this;
		coreApp = self;
	},
	views :["core.test.view.TestGrid"]
});