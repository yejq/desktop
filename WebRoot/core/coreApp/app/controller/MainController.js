/**
*程序主控制器
*/
Ext.define("core.app.controller.MainController", {
	extend : "Ext.app.Controller",
	mixins : {
		btnCtr : "core.app.controller.ButtonController"
	},
	ctr : {},
	init : function(){
		var self = this;
		coreApp = self;
		self.initBtn();
		//注册事件
		this.control(self.ctr);
	},
	views :["core.app.base.BaseGrid"]
});