/**
*按钮主控制器
*/
Ext.define("core.app.controller.ButtonController", {
	extend : "Ext.app.Controller",
	initBtn : function(){
		var self = this;
		var btnCtr = {
			"basegrid button[ref=gridInsert]" : {
				click : function(btn){
					alert("点击了添加按钮")
				}
			},
			"basegrid button[ref=gridEdit]" : {
				click : function(btn){
					alert("点击了编辑按钮")
				}
			},
			"basegrid button[ref=gridDelete]" : {
				click : function(btn){
					alert("点击了删除按钮")
				}
			},
			"basegrid button[ref=gridSave]" : {
				click : function(btn){
					alert("点击了保存按钮")
				}
			}
		}
		Ext.apply(self.ctr, btnCtr);
	}
});