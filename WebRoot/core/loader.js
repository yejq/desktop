//初始化提示控件，以便提示控件可用
Ext.QuickTips.init();
//初始化提示工具框
Ext.tip.QuickTipManager.init();
//启动动态加载机制
Ext.Loader.setConfig({
	enabled : true,
	paths :{
		baseUx : "core/ux/base"
	}
});
//同步加载
Ext.syncRequire([
	"baseUx.form.datetime.DateTimePicker",
	"baseUx.form.datetime.DateTime"
]);