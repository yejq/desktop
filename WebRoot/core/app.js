Ext.onReady(function(){
	Ext.application({
		name : "core",
		scope : this,
		appFolder : "core/coreApp",
		launch : function(){ //当前页面加载完成执行的函数
			
		},
		controllers:[
         "core.app.controller.MainController"          //装载主控制器
        ]
	});
});