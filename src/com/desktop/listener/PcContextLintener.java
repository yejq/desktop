package com.desktop.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.desktop.action.BaseAction;

/**
 * 系统初始化监听器
 * @author zhangshuaipeng
 *
 */
public class PcContextLintener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent arg0) {
		
	}

	public void contextInitialized(ServletContextEvent sc) {
		BaseAction.webrootAbsPath=sc.getServletContext().getRealPath("/");
		BaseAction.absClassPath=PcContextLintener.class.getResource("/").getPath().substring(1);
		
	}

}
