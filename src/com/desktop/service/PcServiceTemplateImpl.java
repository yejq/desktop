package com.desktop.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.desktop.dao.PcDaoTemplate;

@Component("pcServiceTemplate")
public class PcServiceTemplateImpl implements PcServiceTemplate {
	private PcDaoTemplate pcDaoTemplate;

	public Object findById(Class<?> clazz, String id) {
		return pcDaoTemplate.findById(clazz, id);
	}

	public List<?> findAll(Class<?> clazz) {
		return pcDaoTemplate.findAll(clazz);
	}

	public Integer getCount(String hql) {
		return pcDaoTemplate.getCount(hql);
	}

	public List<?> findByPage(Class<?> clazz, String whereSql, int from, int size) {
		return pcDaoTemplate.findByPage(clazz, whereSql, from, size);
	}

	public Object save(Object entity) {
		return pcDaoTemplate.save(entity);
	}

	public Object update(Object entity) {
		return pcDaoTemplate.update(entity);
	}

	public void delete(Object entity) {
		pcDaoTemplate.delete(entity);
	}

	public void deleteBatchById(Class<?> clazz, String idName, String ids) {
		List<?> models = pcDaoTemplate.queryByHql(" from " + clazz.getName() + " where " + idName + " in (" + ids + ")");
		for (Object obj : models) {
			pcDaoTemplate.delete(obj);
		}
	}

	public Object getEntityByHql(String hql) {
		return pcDaoTemplate.getEntityByHql(hql);
	}

	public Long executeSql(String sql) {
		return pcDaoTemplate.executeSql(sql);
	}

	public List<?> queryByHql(String hql) {
		return pcDaoTemplate.queryByHql(hql);
	}

	public List<?> queryBySql(String sql) {
		return pcDaoTemplate.queryBySql(sql);
	}

	public List<?> queryBySql(String sql, Class<?> c) {
		return pcDaoTemplate.queryBySql(sql, c);
	}

	@Resource(name = "pcDaoTemplate")
	public void setPcDaoTemplate(PcDaoTemplate pcDaoTemplate) {
		this.pcDaoTemplate = pcDaoTemplate;
	}

	public List<?> queryByHql(String hql, Integer start, Integer limit) {
		return pcDaoTemplate.queryByHql(hql, start, limit);
	}

	public Long executeHql(String hql) {
		return pcDaoTemplate.executeHql(hql);
	}

	public void executeBatchHql(String[] updateSqls) {
		for (String sql : updateSqls) {
			pcDaoTemplate.executeHql(sql);
		}
	}

}
