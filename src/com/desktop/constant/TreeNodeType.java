package com.desktop.constant;
/**
 * 树形字段类型枚举
 * @author zhangshuaipeng
 *
 */
public enum TreeNodeType {
	   ID,
       TEXT, 
       CODE, 
       CLS,
       HREF,
       HREFTARGET,  
       EXPANDABLE,
       EXPANDED,
       DESCRIPTION, 
       ICON,
       CHECKED,   
       PARENT,
       NODETYPE,
       NODEINFO,    
       NODEINFOTYPE, 
       ORDERINDEX,
       DISABLED;
	   public Boolean equalsType(TreeNodeType other){
		   int i=this.compareTo(other);
		   if(i!=0){
			   return false;
		   }
		   return true;
	   }
}
